import javax.swing.*;
import java.awt.*;

public class MainGui extends JFrame {

	JButton runButton;
	JTextField argumentField;
	MainGraphic gridGraphic;

	public MainGui(){
		JFrame mainFrame = new JFrame("Langton's Ant");
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setSize(400,400);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainFrame.add(mainPanel);

		gridGraphic = new MainGraphic();
		mainPanel.add(gridGraphic, BorderLayout.CENTER);
		runButton = new JButton("Start");
		argumentField = new JTextField("LRRRRRL");


		JPanel lowerPanel = new JPanel();

		lowerPanel.add(argumentField);
		lowerPanel.add(runButton);

		argumentField.setColumns(20);

		mainPanel.add(lowerPanel, BorderLayout.SOUTH);


		mainFrame.setVisible(true);
	}

	public static void main(String[] args){
		new MainGui();
	}
}