import javax.swing.*;
import java.awt.*;

/**
 * Created by Jacob Stuart on 8/2/14.
 */
public class MainGraphic extends JPanel {

	final int rows = 20;
	final int columns = 20;
	public MainGraphic(){

		super();

	}


	public void paint(Graphics g){
		double height = getSize().getHeight();
		double width = getSize().getWidth();
		double individualWidth = width / rows;
		double individualHeight = height / columns;


		g.clearRect(0,0,(int)width, (int)height);

		//horizontal lines
		for (double i = 0; i < height;){
			g.drawLine(0, (int)i, (int)width, (int)i);
			i += individualHeight;
		}

		//vertical lines
		for (double i = 0; i < width;){
			g.drawLine((int)i, 0, (int)i, (int) height);
			i += individualWidth;
		}
	}

	public static void main(String[] args){
		MainGraphic mg = new MainGraphic();

//		mg.setVisible(true);
		JFrame frame = new JFrame();
		frame.add(mg);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}


}
